clear all
close all
clc
%%%--------------- Segmentation by Texture -------------%%% 
% Free parameters: 
% theta: angle step, b: bandwidth, kappa: blurring factor , K : number of
% groups to calculate kNN 
 
% b = 1; 
% kappa = 1; 
Kneigh=4;  % Value for K-means clustering
% Maximum value of k in u_k can be log2(N/4)

% Vary b and kappa 
bs = [0.5,1.0,1.5,2.0,3.0];
kappas = [0.5,1.0,2.0,4.0,4.5];
cols = []; 

for kappa=1:numel(kappas)
        bw=[]; % bandwidth variation with same angle
    for b=1:numel(bs)
        J=doall(b,kappa,Kneigh);   
        %imshow(J); 
        bw = cat(1,bw,J);
    end
    %figure();imshow(bw); title('col 1')
    cols = horzcat(cols,bw); 
end 

figure();imshow(cols);title(sprintf('Segmentation for different values of b and \\kappa for \\theta= 90�'));

function [J] = doall(b,kappa,Kneigh)
%thetas = [0,pi/4,pi/2,3*pi/4]; 
thetas = (90*pi/180);
% Step 1: multichannel decomposition with a bank of Gabor filters
I = imread('Tarea4/images/river.tif'); 
[M,N]=size(I);
kmax = log2(floor(N/4));

u_k = (2.^(0:kmax))*sqrt(2); %<<<<<< AQUI VECTOR DE FREC RADIALES

% Apply FFT to the original image 
If = fft2(I);
If = fftshift(If);%Shift zero-frequency component to center of spectrum

GaussBank = []; %%%%%%NEW

%SS = []; % images softened with Gaussian Filter
set_ = []; % The set of images with Gaussian Filters. A cube of M x N x number texture channels
cols = [];
cols_soft = []; 
GG = zeros(M,N);
for i = 1:numel(u_k)
    JJ = [];
    SS = []; % images softened with Gaussian Filter
    GaussFil=GaussFilter(M,N,u_k(i),kappa);
    GaussBank = cat(2,GaussBank,GaussFil); 
    %SS = cat(2,SS,mat2gray(GaussFil));
    for j=1:numel(thetas)
        GF = GaborFilter(M,N,u_k(i),thetas(j),b); 
        newimg= If.*GF; %<<<<<< .* MULTIPLICACION PUNTUAL
        GG = GF + GG;   
        softimg = newimg.*GaussFil;
        newimg=ifftshift(newimg);
        textureChannel = ifft2(newimg);
        JJ = cat(1,JJ,mat2gray(abs(textureChannel)));
        %figure();imshow(mat2gray(abs(textureChannel)));
        %softimg = newimg.*GaussFil;
        softimg = ifftshift(softimg);
        softimg = ifft2(softimg);
        SS = cat(1,SS,mat2gray(abs(softimg)));
        set_ = cat(3,set_,mat2gray(abs(softimg))); % To pass for texture channel selection
    end
    cols = horzcat(cols,JJ); 
    cols_soft = horzcat(cols_soft,SS);
end 
%figure();imshow(JJ); title('Multi-channel decomposition for feature enhancement using Gabor Filters');

% figure();imshow(mat2gray(GG));title('Gabor Filter Bank');
%  figure();imshow(cols_soft); title('Multi-channel decomposition with Gaussian Filter')
%  figure();imshow(mat2gray(GaussBank));title('Gaussian Filters');
%  figure();imshow(mat2gray(cols)); title('Multi-channel decomposition for feature enhancement using Gabor Filters');

% Fourier transform of texture channel 
% textchF = fft2(textureChannel);
% textchF = fftshift(textchF);%Shift zero-frequency component to center of spectrum

subChannels = selchannels(set_);
%subChannels = mat2gray(subChannels);
[~,~,sels] = size(subChannels); 
selectedimgs = [];
for i=1:sels 
    selectedimgs = cat(3,selectedimgs,subChannels(:,:,i)); % change to 2 for visualization
end 
%figure();imshow(selectedimgs);title('Selected Texture Channels');



J=grouping(selectedimgs,Kneigh); 
%imshow(J);
    
%figure(); imshow(result); title('Selected Channels')
%%%% TEXTURE CHANNEL SELECTION %%%% 
function subset_ = selchannels(set_)
[M,N,K] = size(set_); 
% Sum of all texture channels 
sxy = sum(set_,3);
% Total sum squared 
ST = sum(sxy(:).^2);
% Step 1: select the best texture channel from the original set
[Rmax, id] = R(set_,sxy,ST,M,N,K); % Determines the channel that maximizes R 
subset_ = set_(:,:,id); % Initialized the subset
set_(:,:,id) = []; % Eliminates the best channel from the original subset 
K = K-1; 
% Step 2: select the best chanels from the remaining subset 
while Rmax <= 0.95 && K > 0
    saux1 = sum(subset_,3);
    sxyh = saux1(:,:,ones(1,K))+set_; % Sums all the images of the subset
    [Rmax,id] = R(sxyh,sxy,ST,M,N,K); % Determines the channel that maximizes R 
    subset_ = cat(3,subset_,set_(:,:,id)); % Adds the best to the subset
    set_(:,:,id) = []; % Eliminates the best channel from the original subset
    K = K-1;
end
end 
%--------------------------------
function [Rmax,id] = R(A,B,ST,M,N,K)
SSE_ = sum(reshape((A-B(:,:,ones(1,K))).^2,M*N,K),1); % Error
[Rmax,id]= max(1-(SSE_/ST)); % Channel that maximized R 
end
%--------------------------------
% Grouping up similar textures 
function J = grouping(G,K)
[N,M,d]= size(G);
MN = M*N;
% Adds information about adjacency
[x,y] = meshgrid(0:M-1,0:N-1);
X = cat(3,G,x,y);
% Converts to column vectors
X = reshape(X,MN,d+2);
% Softmax normalization
m = mean(X,1);
s = std(X,[],1);
XN = (X-m(ones(1,MN),:))./s(ones(1,MN),:); 
% k-means clustering 
opts = statset('MaxIter',1000);
labels = kmeans(XN,K,'EmptyAction','singleton','Replicates',5,'Options',opts);
% Transforms labels to image
J = reshape(labels,N,M,1);
J = label2rgb(J);
end 


% Calculating H(u,v):
function [H]= GaborFilter(M,N,u_k,theta,b)
%u_k = (2^(k))*sqrt(2); 
sigma_b = (1/(2*u_k))*sqrt(log(2)/2)*((2^(b)+1)/(2^(b)-1));
sigma_u = 1/(2*pi*sigma_b); 
H = zeros(M,N); 
c_u = M/2;
c_v = N/2;
for u=1:M-1
    for v=1:N-1
        uhat = ((u-1)-c_u)*cos(theta) + ((v-1)-c_v)*sin(theta); %<<<<< CENTRALIZAR
        vhat= ((v-1)-c_v)*cos(theta) - ((u-1)-c_u)*sin(theta);
%          uhat = u*cos(theta) + v*sin(theta); 
%          vhat = v*cos(theta) - u*sin(theta);        
        comp1 = ((uhat-u_k)^2/sigma_u^2)+(vhat^2/sigma_u^2);
        comp2 = ((uhat+u_k)^2/sigma_u^2)+ (vhat^2/sigma_u^2);
        H(u,v) = exp(-0.5*comp1)+exp(-0.5*comp2);
        
    end 
end 
end


function [H] = GaussFilter(M,N,u_k,kappa) 
H = zeros(M,N);
% Frequency coordinates displaced to the center of the plane
sigma2 = kappa*(N/u_k)^2;
for u=1:M
    for v=1:N
       % H(u,v) = u^2+v^2; 
       ucent = (u-1)-M/2; u2 = ucent.*ucent;
       vcent = (v-1)-N/2; v2 = vcent.*vcent; 
       H(u,v)=exp(-(u2+v2)/(2*sigma2)); % Gaussian Kernel 
    end
end 
end 

end

