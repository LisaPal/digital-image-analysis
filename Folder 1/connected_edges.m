clear all
close all
clc

% Label connected components in a binary image
[labelled, numlabels] = rosenpfaltz('images01/fruit.png') ;
[height, width] = size(labelled); 

% Get the boundary for all connected component labels in a single image
for k=1:numlabels
    image = zeros(height,width);
    for i=1:height
        for j=1:width
            if labelled(i,j)==k
                image(i,j)=1; 
            end

        end
    end 
    filename=sprintf('component_%d.png',k); 
    imwrite(image,filename); 
    [edge_vec,bound_img]=boundary(filename); 
    imshow(bound_img);
    x = (edge_vec(:,1));
    y = (edge_vec(:,2));
    % Plot the points in the vector containing the points in the boundary
    f=scatter(y,x,'filled');
    axis equal; 
    set(gca, 'YDir','reverse')
    scattername = sprintf('component_%d',k); 
    saveas(f, filename);
    
end
