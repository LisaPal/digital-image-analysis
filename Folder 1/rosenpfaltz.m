function [newimage,final_labelnum] = rosenpfaltz(imgname)
% % Load image to use
I= imread(imgname) ;
% Get image dimensions
[height,width] = size(I);
% eq_rel is the matrix where we register the equivalence of 2 pixel labels 
n = 2000;
eq_rel = zeros(n,n);
[h,w] = size(eq_rel);
img_pre=zeros(height,width);
for i=1:height
  for j=1:width
    img_pre(i,j)=I(i,j); 
  end
end

img = padarray(img_pre,[1,1],0);

% Label propagation algorithm 
label = 0;
total_labels = 0; 
% lp = 0;
% lq = 0;
for i=2:height
    for j=2:width
            if img(i,j) == 1
                lp = img(i - 1, j);
                lq = img(i, j - 1);
                if lp == 0 && lq == 0                                
                    label = label + 1; 
                    total_labels = label;
                    lx = label;
                elseif ((lp ~= lq) && (lp ~= 0) && (lq ~= 0))                                        
                    eq_rel(lp, lq) = 1; %Registrar equivalencia (lp, lq)
                    lx = lp;                    
                elseif lq ~= 0
                    lx = lq;
                elseif lp ~= 0
                    lx = lp;                
                end
                img(i,j) = lx;
           end
     end
end

% Getting B0: original matrix with equivalencies
% They are already in eq_rel, but we reduce the size to the number of
% labels used, which we only know after the label propagation algoritm
B0 = zeros(total_labels,total_labels);
for i=1:total_labels
      for j=1:total_labels 
          B0(i,j)=eq_rel(i,j); 
      end
end

% Generating B: the binary matrix of equivalence relations, ensuring the
% symmetry and reflexivity properties.
% B = zeros(total_labels,total_labels);
[B01,B02] = size(B0)
B = zeros(B01,B02);
B = B0 | transpose(B0) | eye(total_labels);

% Getting B+, the transitive closure matrix
[M N] = size(B); 
Bplus = zeros(M,N);
Bplus = B;     
for k=1:N
      for i=1:N
            for j=1:N
                Bplus(i,j) = Bplus(i,j)|(Bplus(i,k)&Bplus(k,j));
            end
      end
end

C=unique(Bplus,'rows');
final_labelnum = size(C,1); 
newimage = zeros(height,width);
for i=1:height
    for j=1:width
        if img(i,j)~= 0
            col = img(i,j); %the label of image is the column of the C array we look at
            %fprintf('col %d ', col)
            newlabel = find(any(C(:,col)==1,2));
            %fprintf('newlabel %d',newlabel)
            newimage(i,j)=newlabel; 
        end
    end 
end 

RGB = label2rgb(newimage);%,'spring','c','shuffle'); 
%return RGB
%figure
imshow(RGB);
end 




