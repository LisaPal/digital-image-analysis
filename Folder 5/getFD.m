function [X,Y] = getFD(P)
% Input: P - the number of fourier descriptors to get. 
% For all the subfolders in folder 'datos', get the images within them and
% their label, which is the subfolder name. 
    [Files,Labels] = getFilesandLabels('datos');
    num_files = length(Files);
    X = zeros(num_files,2*P+1); % 2*P because on input P, we consider P points to the right and P to the left of the spectrum
    Y = Labels;
    for k = 1:num_files
       X(k,:) = fourier_descriptor(Files{k},P);
    end
end
