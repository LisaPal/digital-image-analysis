function [hu_moments_vector_dyn] = moments(filename)
img = imread(filename);    %'datos/turtle/turtle-1.gif'
img=im2double(img);
[N, M] = size(img);
xgrid = repmat((-floor(N/2):1:ceil(N/2)-1)',1,M);
ygrid = repmat(-floor(M/2):1:ceil(M/2)-1,N,1);

[x_bar, y_bar] = centerOfMass(img,xgrid,ygrid);

% normalize coordinate system by subtracting mean
xnorm = x_bar - xgrid;
ynorm = y_bar - ygrid;

% Central moments
mu_11 = central_moments( img ,xnorm,ynorm,1,1);
mu_20 = central_moments( img ,xnorm,ynorm,2,0);
mu_02 = central_moments( img ,xnorm,ynorm,0,2);
mu_21 = central_moments( img ,xnorm,ynorm,2,1);
mu_12 = central_moments( img ,xnorm,ynorm,1,2);
mu_03 = central_moments( img ,xnorm,ynorm,0,3);
mu_30 = central_moments( img ,xnorm,ynorm,3,0);

% Calculate Hu's Invariant moments
%central_moment = [mu_11, mu_20, mu_02, mu_21, mu_12, mu_03, mu_30];
%calculate first 7 hu moments of order 3
hu_one   = mu_20 + mu_02;
hu_two   = (mu_20 - mu_02)^2 + 4*(mu_11)^2;
hu_three = (mu_30 - 3*mu_12)^2 + (mu_03 - 3*mu_21)^2;
hu_four  = (mu_30 + mu_12)^2 + (mu_03 + mu_21)^2;
hu_five  = (mu_30 - 3*mu_12)*(mu_30 + mu_12)*((mu_30 + mu_12)^2 - 3*(mu_21 + mu_03)^2) + (3*mu_21 - mu_03)*(mu_21 + mu_03)*(3*(mu_30 + mu_12)^2 - (mu_03 + mu_21)^2);
hu_six   = (mu_20 - mu_02)*((mu_30 + mu_12)^2 - (mu_21 + mu_03)^2) + 4*mu_11*(mu_30 + mu_12)*(mu_21 + mu_03);
hu_seven = (3*mu_21 - mu_03)*(mu_30 + mu_12)*((mu_30 + mu_12)^2 - 3*(mu_21 + mu_03)^2) + (mu_30 - 3*mu_12)*(mu_21 + mu_03)*(3*(mu_30 + mu_12)^2 - (mu_03 + mu_21)^2);



% Dynamic range reduction
hu_moments_vector = [hu_one, hu_two, hu_three,hu_four,hu_five,hu_six,hu_seven];
hu_moments_vector_dyn= -sign(hu_moments_vector).*(log10(abs(hu_moments_vector)));
hu_moments_vector;
hu_moments_vector_dyn;
% allOneString = sprintf('%.5f,' , hu_moments_vector_dyn);
% allOneString = allOneString(1:end-1);% strip final comma
% allOnestring = sprintf('[%.5f]',allOneString);

%out=['[' num2str(allOneString) ']']
%out=['[' num2str(hu_moments_vector_dyn) ']']
% Centers of mass of the object 
function [x_bar, y_bar] = centerOfMass(img,xgrid,ygrid)

    eps = 10^(-6); % very small constant 
    
    x_bar = sum(sum((xgrid.*img)))/(sum(img(:))+eps);
    y_bar = sum(sum((ygrid.*img)))/(sum(img(:))+eps);
end
% Central moments
function cm = central_moments( img ,xnorm,ynorm,p,q)
    
    cm = sum(sum((xnorm.^p).*(ynorm.^q).*img));
    cm_00 = sum(sum(img)); %this is same as mu(0,0);
    % normalise moments for scale invariance
    cm = cm/(cm_00^(1+(p+q)/2));
    
end

% Seven invariant moments (Hu' moments) 
end 


