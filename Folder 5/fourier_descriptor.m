function [descriptor] = fourier_descriptor(filename,P)
%filename = 'datos/turtle/turtle-1.gif';
img = imread(filename);
[height,width]= size(img); 

% This has all the points (x,y) in the edge of the shape
edgeVector = getEdgePoints(filename);
[K,~] = size(edgeVector);  % K is the number of points in the edgeVector
% s has a list of imaginary abers, coming from the edge points 
s=complex(edgeVector(:,1),edgeVector(:,2));
a = fft(s);
a = fftshift(a);%Shift zero-frequency component to center of spectrum
% P : the aber of coefficients we are using to reconstruct the image

[totalEdgePts,~] = size(edgeVector);
magnitude = abs(a); 
plotpts = (-totalEdgePts/2+1:1:totalEdgePts/2)'; 
%figure();semilogy(plotpts',magnitude); title('Frequency vs. Magnitude semilog plot')
%P= 5;%totalEdgePts/4;
middlepos = round(size(a)/2); 
middlepos = middlepos(1); 
rightzero=(middlepos+P+1);
newa=zeros(size(a));
newa(middlepos-P:middlepos+P)=a(middlepos-P:middlepos+P); 
a(rightzero:numel(a)) = 0;
a(1:middlepos-P-1)=0; 
                            %a(P+1:size(a))=0; % 
%figure(); plot(plotpts',abs(a)); 
%figure();semilogy(plotpts',abs(a)); title('Frequency vs. Magnitude semilog plot with P points')

center_a = round(numel(a)/2); 
% Coefficient normalization for translation
%a(1)=0;
 a(center_a)=0; 
% %a(1) = a(1)+K*s(1); 
% % Coefficient normalization for scaling 
 %get max absolute value in a , which should be equivalent to a(1) before
 %shifting
 %nonshifted = ifftshift(a); 
 
 a_abs=abs(a);
 [maxval,maxpos] = max(a_abs); 
 a = a./a(center_a+1); % a(1) in slides = a(2) for me because matlab has no 0-indexing. 
% Coefficient normalization for rotation
a = abs(a); 

descriptor=zeros(1,2*P); 
descriptor=a(middlepos-P:middlepos+P); 
% Plot Fourier descriptor
%figure(); plot(plotpts',abs(a));
a = ifftshift(a);
reconstructed = ifft(a);
% descriptor=zeros(1,2*P); 
% descriptor=reconstructed(middlepos-P:middlepos+P); 
towrite = reconstructed'; 

reconstr_img =zeros(size(edgeVector));
%reconstr_img(:,:) =[round(real(reconstructed(:))), round(imag(reconstructed(:)))]; 
reconstr_img(:,:) =[real(reconstructed(:)),imag(reconstructed(:))]; 


%  filename=sprintf('component_%d.png',k); 
%     imwrite(image,filename); 
% [edge_vec,bound_img]=boundary(filename); 
%imshow(bound_img);
x = (reconstr_img(:,1));
y = (reconstr_img(:,2));

% Plot the points in the vector containing the points in the boundary
% figure();
% f=scatter(y,x);
% f.Marker = '.'
% title('Reconstructed');
% axis equal; 
% set(gca, 'YDir','reverse') 
% 
% %set(gca, 'XDir','reverse')
% scattername = sprintf('Reconstructed'); 
% %saveas(f, filename);
% 
% %The original image scatter
% figure();
% f=scatter(edgeVector(:,2),edgeVector(:,1));
% f.Marker = '.'
% title('Original');
% %axis equal; 
% set(gca, 'YDir','reverse')
% %set(gca, 'XDir','reverse')

%Reconstructed


% 
% % Original 
% boundary_img = zeros(height,width); 
% for p = 1:size(reconstr_img)
%     boundary_img(edgeVector(p,1),edgeVector(p,2)) = 1;
% end
% figure();imshow(boundary_img);title('Original');
% 
% 
% % % Drawing the reconstructed image 
% myimage = zeros(height,width); 
% for p = 1:size(reconstr_img)
%     myimage(reconstr_img(p,1),reconstr_img(p,2)) = 1;
% end
% 
% figure();imshow(boundary_img); title('Reconstructed');
end 