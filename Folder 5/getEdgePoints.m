function edge_vec = getEdgePoints(filename)
%clear all
close all
clc
% Define position code : see function nextpixel near end of file 
% right = 0, above = 1, left = 2, below = 3

% Load image to use
%filename = 'datos/turtle/turtle-1.gif'; 
img= imread(filename);
% Padding array to avoid boundary of object being in boundary of image.  
I = padarray(img,[1 1],0);
%imshow(I)
[height,width] = size(I);
% Getting first pixel, the top-leftmost white pixel
p0 = getfirstpix(I);
% Where we will put the image of the boundary only
boundary_img = zeros(height,width); 
% find the array which contains all the points in the boundary of the image
edge_vec = ccfind(I,p0);

[numpoints,numcoor] = size(edge_vec); % x is the number of points in the edge
for p = 1:numpoints
    boundary_img(edge_vec(p,1),edge_vec(p,2)) = 1;
end

%newfilename = extractBetween(filename,'turtle/','.gif'); 
imwrite(boundary_img,'contour_plane.png');

% Given an image, search from the top-left to find the first white pixel 
function[p0_pos] = getfirstpix(I)
    [height,width] = size(I);
    for i=1:height
        for j=1:width
             if I(i,j)~= 0 
                p0_pos = [i j];
                return
             end
        end 
    end 
end 


% Given a pixel and a number, return a pixel associated with the 4-neighborhood of the
% current pixel
function[newpos] = nextpixel(pi,number)
    row = pi(1);
    col = pi(2);
    if number == 0
        newpos=[row, col+1];
    elseif number == 1
        newpos = [row-1,col];
    elseif number == 2
        newpos = [row, col-1];
    elseif number == 3
        newpos = [row+1,col];
    end

end

% Given the image and its starting pixel coordinates, find all the points
% in image that are in its boundary
function [edge] = ccfind(I,p0)
    d = 0;
    edge = p0;
    n = 1;
    while 1
        last = edge(n,:);
        q = mod(d+3,4);
        new = nextpixel(last,q);
        while I(new(1),new(2)) == 0
            q  = mod(q+1,4);
            new = nextpixel(last,q);
        end
        d = q;
        if new == p0
            break
        end
        edge = [edge ; new];
        n = n+1;
    end

end

end 


