function [X,Y] = getHu( )
% For all the subfolders in folder 'datos', get the images within them and
% their label, which is the subfolder name. 
    [Files,Labels] = getFilesandLabels('datos');
    num_files = length(Files);
    X = zeros(num_files,7);
    Y = Labels;
    for k = 1:num_files
       X(k,:) = moments(Files{k});
    end
end

