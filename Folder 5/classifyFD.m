function [accuracy, classes] = classifyFD(k,P)
% Input: P - the number of fourier descriptors. 
%        k - number of neighbours. 
% Object classification using KNN algorithm on Fourier Descriptors. 
 [X,Y] = getFD(P);
 % fitcknn: returns a k-nearest neighbor classification model based on the input variables 
    %(also known as predictors, features, or attributes) in the table Tbl and output (response) Tbl.ResponseVarName.
    mdl = fitcknn(X,Y,'NSMethod','exhaustive','NumNeighbors',k,'Distance','correlation');
    % crossval:performs 10-fold cross-validation for the function fun, applied to the data in X
    cvloo = crossval(mdl,'Leaveout','on'); % Leaves one out 
    closs = kfoldLoss(cvloo); 
    
        num_file = size(X,1);  % corresponding to each file 
    label_predic = cell(size(Y));  % The resulting labels for each file 
    for j = 1:num_file
        % Doing the leave-one-out part 
        X_loo = X(1:end ~= j,:);
        Y_loo = Y(1:end ~= j);
        MDP = fitcknn(X_loo,Y_loo,'NSMethod','exhaustive','NumNeighbors',k,'Distance','correlation');
        %predict :returns a vector of predicted class labels for the predictor data in X
        label_predic(j) = predict(MDP,X(j,:));
    end
    correct = strcmp(label_predic,Y);
    num_correct = sum(correct);
    accuracy = num_correct/num_file;
    
    classes = unique(Y);
    num_classes = length(classes);
    CorrectByClass = zeros(1,num_classes);
    for j = 1:num_classes
        indexes = strcmp(Y,classes{j});
        CorrectByClass(j) = sum(strcmp(label_predic(indexes),Y(indexes)))/sum(indexes);
    end
    classes = [classes;num2cell(CorrectByClass)]; 
end




