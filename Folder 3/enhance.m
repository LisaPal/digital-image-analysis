clear all
close all
clc

original = imread('Material_tarea03/images/original/thorax.tif');
%figure;imshow(original);title('Original Image');

I = imread('Material_tarea03/images/noisy/thorax_noisy.tif'); 
noisy=I;
%imshow(I);title('Noisy Image')
%I=im2double(I)*255;
[M,N]=size(I);
%If = fft2(log(I+1)); % these are complex doubles 
If = fft2(I);
If=fftshift(If);%Shift zero-frequency component to center of spectrum



% Calculating D(u,v): 
D = zeros(M,N); 
for u=1:M
    for v=1:N
        D(u,v) = ((u-(M/2))^2 + (v-N/2)^2)^0.5;
    end 
end 

% Gaussian Filter 
D0 =40; 
n=2; 
alpha = 3; %thorax 0.2
beta = 0.97;  %thorax 0.5


H_gauss = getHgauss(D,D0);
H_butter = getButterworth(D,D0,n);


H_butter_enh = getEnhanced(H_butter,alpha,beta);
H_gauss_enh = getEnhanced(H_gauss,alpha,beta);
%plot(1:M,H_gauss(:,floor(N/2)));
[X,Y] = meshgrid(1:N,1:M);
figure();s=surf(X,Y,H_butter);title('H(u,v) for Butterworth Filter');xlabel('v');ylabel('u');
s.EdgeColor = 'none';

%figure(); k=surf(X,Y,(real(log(If))));title('Magnitud del Espectro para Chest X-ray');xlabel('v');ylabel('u');
k.EdgeColor = 'none';

% If= If.*H_gauss;
% If_enh = If.*H_gauss_enh;
If = If.*H_butter;
%If = log(If+1);
If_enh = If.*H_butter_enh;
%Inverse zero-frequency shift 
If=ifftshift(If);
I=real(ifft2(If));
%figure,imshow(I,[0 255]);title(['filtered blurred image with D0 =', num2str(D0)])

If_enh=ifftshift(If_enh);
I2=abs(ifft2(If_enh));

%------------------- PRINTING IMAGES SIDE BY SIDE
% figure(),imshow(I2,[0 255]);title(['enhanced image with \alpha=', num2str(alpha),' and \beta=', num2str(beta)])
% 
% imagearray=[original,noisy,I,I2];
% 
% figure();
% montage(imagearray);
%-------------------

%%%%%% START OF HOMOMORPHIC FILTER %%%%%%
% Step 1: take ln
%I_ln=I;
I_ln = log(I);  % OR : im2double(original)
% Step 2: FFT 
I_lnfft= fft2(I_ln); % these are complex doubles 
I_lnfft=fftshift(I_lnfft);%Shift zero-frequency component to center of spectrum
% Step 3: Apply H_en(u,v) for enhancement. 
%homoHGauss= getHgauss(D,D0);
%homo_Ienh = getEnhanced(homoHGauss,alpha,beta);
homoHButter = getButterworth(D,D0,n);
homo_Ienh = getEnhanced(homoHButter,alpha,beta);
homo_Ienh = I_lnfft.*homo_Ienh; 
% Step 4: Do inverse FFT and then take the exponential
%Inverse zero-frequency shift 
homoI=ifftshift(homo_Ienh);
inv2 = ifft2(homoI);
%homoI = real((inv));
homoI=abs(exp(inv2)); % OR inv
%figure;imshow(homoI,[0 255]);title(['enhanced image with \alpha=', num2str(alpha),' and \beta=', num2str(beta)])
imagearray=[original,noisy,I,homoI];
montage(imagearray);


function [H_gauss] = getHgauss(D,D0)
H_gauss = exp(-1*(D.^2)/(2*(D0^2))); 
end

% nth order Butterworth filter
function [H_butter] = getButterworth(D,D0,n)
H_butter = 1./(1.+((D./D0).^(2*n))); 
end


% Enhance any of the filters (Gaussian or Butterworth) 
function [H_Butterenh] = getEnhanced(H,alpha,beta)
H_Butterenh = alpha*(1-H)+beta;
end 


