function [ nearestCenters ] = nearest_centres(coord,centers)
if (coord<centers(1))
    nearestCenters= centers(1);
    return
end 
for i = 2:length(centers)
        if coord < centers(i)
            nearestCenters = [centers(i-1),centers(i)];
            return
        end
end

nearestCenters = centers(end); 

end

