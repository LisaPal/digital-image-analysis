function [newimg] = bilinear_interpol(img,xs,ys,clip)

% img= imread(image) in main
% xs are the divisions that make up columns
% ys are the divisions that make up the rows

%height,width
[rows,columns,z] = size(img);

%new image:
newimg = zeros(rows,columns);
%centersX/centersY is the array with the x/y coords of the centers
centersX = zeros(1,length(xs)-1); 
centersY = zeros(1,length(ys)-1); 

for i=1:length(xs)-1
    centersX(i) = floor((xs(i)+ xs(i+1))/2); %column coordinate
end  

for i=1:length(ys)-1
    centersY(i) = floor((ys(i)+ys(i+1))/2); %row coordinate
end

% Make a cell array WindowMap that contain mapping funcion T(k) for each
% window.
windowMap = cell(length(centersY),length(centersX)); 
for i=1:length(centersY)
    for j=1:length(centersX)
        window= getWindow(img,centersY(i),centersX(j),xs,ys);
        windowMap(i,j) = {windowHist(window,clip)}; 
    end 
end 

%For every pixel in the image, we must find its nearest neighbors.
% x and y coordinates of points are considered separately

for thisrow=1:rows  % height (y)
    for thiscol=1:columns  %width (x)
        pixI = img(thisrow,thiscol)+1; % pixel intensity
        row_neighbours= nearest_centres(thisrow,centersY);
        col_neighbours= nearest_centres(thiscol,centersX);
        
        % Check in which region the current pixel is and interpolate
        % accordingly
        
        %RI - there are 4 distinct neighbours 
        if(length(row_neighbours)==2 && length(col_neighbours)==2)
        
            % Center pixel coordinates of region A: 
            Acenter = [row_neighbours(1),col_neighbours(1)]; 
            % Relative position of window A in the image: 
            A = [find(centersY==Acenter(1)),find(centersX==Acenter(2))];
            %Acenter= [centersY(TA(1)),centersX(TA(2))];
            TA = windowMap{A(1),A(2)};
            
            
            Bcenter = [row_neighbours(1),col_neighbours(2)]; 
            B = [find(centersY==Bcenter(1)),find(centersX==Bcenter(2))];
            TB = windowMap{B(1),B(2)};
            
            Ccenter = [row_neighbours(2),col_neighbours(1)]; 
            C = [find(centersY==Ccenter(1)),find(centersX==Ccenter(2))];
            TC = windowMap{C(1),C(2)};
            
            Dcenter = [row_neighbours(2),col_neighbours(2)]; 
            D = [find(centersY==Dcenter(1)),find(centersX==Dcenter(2))]; 
            TD = windowMap{D(1),D(2)};
            
            x = (thiscol-Acenter(2))/(Bcenter(2)-Acenter(2)); %x
            y = (thisrow-Acenter(1))/(Ccenter(1)-Acenter(1)); %y
            
           newimg(thisrow,thiscol)= floor((1-y)*((1-x)*TA(pixI)+x*TB(pixI))+ y*((1-x)*TC(pixI)+x*TD(pixI)));
                        
            
           % top or bottom RB
            elseif length(row_neighbours) == 1 && length(col_neighbours) == 2
                Acenter = [row_neighbours(1),col_neighbours(1)]; % right window 
                A = [find(centersY==Acenter(1)),find(centersX==Acenter(2))];
                TA = windowMap{A(1),A(2)};
               
                Bcenter = [row_neighbours(1),col_neighbours(2)]; % left window 
                B = [find(centersY==Bcenter(1)),find(centersX==Bcenter(2))];
                TB = windowMap{B(1),B(2)};

                x = (thiscol - Acenter(2))/(Bcenter(2)-Acenter(2));
                newimg(thisrow,thiscol) = floor((1-x)*TA(pixI) + x*TB(pixI));
            
            % left or right RB
             elseif length(row_neighbours) == 2 && length(col_neighbours) == 1
                Acenter = [row_neighbours(1),col_neighbours(1)]; % top window
                A = [find(centersY==Acenter(1)),find(centersX==Acenter(2))];
                TA = windowMap{A(1),A(2)}; 
                
                Ccenter = [row_neighbours(2),col_neighbours(1)]; % bottom window
                C = [find(centersY==Ccenter(1)),find(centersX==Ccenter(2))];
                TC = windowMap{C(1),C(2)};
                
                
                y = (thisrow - Acenter(1))/(Ccenter(1)-Acenter(1));
                newimg(thisrow,thiscol) = floor((1-y)*TA(pixI) + y*TC(pixI));
              %corners  
              elseif length(row_neighbours) == 1 && length(col_neighbours) == 1
                Acenter = [row_neighbours(1),col_neighbours(1)]; % the same window
                A = [find(centersY==Acenter(1)),find(centersX==Acenter(2))];
                TA = windowMap{A(1),A(2)}; 
                
                newimg(thisrow,thiscol) = TA(pixI);
        end     
    end         
end 


imshow(newimg, [0 255]);


function [ck] = acc_distr(vals, occs,k)
ck = sum(occs(1:floor(k))); 
end

% Mapping function
% The accumulated distribution function scaled times (L-1) 
function [tk] = map(vals,occs,k)
L= 256;
ck=acc_distr(vals, occs,k); 
tk = (L-1)*ck; 
end 

end 





