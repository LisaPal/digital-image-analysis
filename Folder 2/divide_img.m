%Input: image, number of row divisions, number of column divisions
function [ xs,ys ] = divide_img( image,Rx,Ry)
[height,width] = size(image);
xwinsize = floor(width/Rx)
ywinsize = floor(height/Ry)

xs = ones(1,Rx+1); 
ys = ones(1,Ry+1); 

xs(Rx+1) = width;
ys(Ry+1) = height; 

for i=2:Rx
    i 
    xs(i) = (i-1)*xwinsize;
end 

for i=2:Ry
    ys(i) = (i-1)*ywinsize;
end 
    
end

