function [imgWindow] = getWindow(img,center_row,center_col,xs,ys)
%Given a complete image and a center point, returns the window of the image
%which has the given center point . xs and ys are the divisions of the
%original image into regions. 
%xs correspond to column divisions, 
%ys to row divisions. 

for row = 1:length(ys)-1 
    if ys(row)<=center_row && center_row<=ys(row+1)
        centerTop = ys(row);
        centerBottom = ys(row+1); 
    end 
end 

for col = 1:length(xs)-1
    if xs(col)<=center_col && center_col<=xs(col+1)
        centerLeft = xs(col);
        centerRight = xs(col+1); 
    end 
end 

% submatrix = fullMatrix(row1:row2, column1:column2);
imgWindow = img(centerTop:centerBottom, centerLeft:centerRight);
%imshow(imgWindow);

       
end

