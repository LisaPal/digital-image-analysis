function [ rgb ] = hsitorgb(hsi)
% hsi is an image, not file 
% Hue, Saturation and Intensity components
 %figure,imshow(hsi);title('HSI Image'); 

 H1=hsi(:,:,1);  
 S1=hsi(:,:,2);  
 I1=hsi(:,:,3);
 
 H1=H1*360; % We will represent angles in degrees [0,360], instead of radians.
 H1 = double(H1);
 S1 = double(S1);
 I1 = double(I1);
 
 %Values of R,G and B will be set depending on 3 cases
 
 R1=zeros(size(H1));  
 G1=zeros(size(H1));  
 B1=zeros(size(H1));  
 RGB1=zeros([size(H1),3]); 
 
 % Case 1: 0<=H<120
 
 %RG Sector(0<=H<120)  
 %When H is in the above sector, the RGB components equations are  
  
 B1(H1<120)=I1(H1<120).*(1-S1(H1<120));  
 R1(H1<120)=I1(H1<120).*(1+((S1(H1<120).*cosd(H1(H1<120)))./cosd(60-H1(H1<120))));  
 G1(H1<120)=3.*I1(H1<120)-(R1(H1<120)+B1(H1<120));  
 
 % Case 2: 120<=H<240
 
 H2=H1-120;  
  
 R1(H1>=120&H1<240)=I1(H1>=120&H1<240).*(1-S1(H1>=120&H1<240));  
 G1(H1>=120&H1<240)=I1(H1>=120&H1<240).*(1+((S1(H1>=120&H1<240).*cosd(H2(H1>=120&H1<240)))./cosd(60-H2(H1>=120&H1<240))));  
 B1(H1>=120&H1<240)=3.*I1(H1>=120&H1<240)-(R1(H1>=120&H1<240)+G1(H1>=120&H1<240)); 
 
 % Case 3: 240<=H<=360
 
 H2=H1-240;  
   
 G1(H1>=240&H1<=360)=I1(H1>=240&H1<=360).*(1-S1(H1>=240&H1<=360));  
 B1(H1>=240&H1<=360)=I1(H1>=240&H1<=360).*(1+((S1(H1>=240&H1<=360).*cosd(H2(H1>=240&H1<=360)))./cosd(60-H2(H1>=240&H1<=360))));  
 R1(H1>=240&H1<=360)=3.*I1(H1>=240&H1<=360)-(G1(H1>=240&H1<=360)+B1(H1>=240&H1<=360));
 
 
 RGB1(:,:,1)=R1;  
 RGB1(:,:,2)=G1;  
 RGB1(:,:,3)=B1;  


    
 %Represent the image in the range [0 255]  
  
 %RGB1 =  floor(255*RGB1); 
 RGB1=im2uint8(RGB1);
 rgb = RGB1;
 %figure,imshow(RGB1);title('RGB Image');



end

