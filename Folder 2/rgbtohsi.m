function [ hsi ] = rgbtohsi( rgb )
% Read an RGB image file.
%rgb = imread(rgbfile);
I = double(rgb)/255; % Normalize the image to have values in [0,1] 
R = I(:,:,1);
G = I(:,:,2);
B = I(:,:,3);

% Obtain value of theta
numtheta= 1/2*((R-G)+(R-B));
denomtheta = ((R-G).^2+((R-B).*(G-B))).^0.5; 

theta = acosd(numtheta./(denomtheta+0.0000001)); % adding a small number to avoid division by 0 error
% Hue 
H = theta; % if B<=G

%If B>G then H= 360-Theta
H(B>G)=360-H(B>G);

%Normalize to the range [0 1]
H=H/360;


% Saturation 
S=1-(3./(sum(I,3)+0.000001)).*min(I,[],3);

%Intensity
I=sum(I,3)./3;  % sum(I,3) = R+G+B; 


%HSI
HSI=zeros(size(rgb));
HSI(:,:,1)=H;
HSI(:,:,2)=S;
HSI(:,:,3)=I;



figure,imshow(HSI);title('HSI Image');
hsi = cat(3,H,S,I);
%hsitorgb(HSI); 

end

