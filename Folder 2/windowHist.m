function [ finalhist ] = windowHist(img,clip)
%windowHist returns equalized, clipped histogram
    L = 256; 
    [y,x] = size(img);
    clippedhist = clippedHist(img,clip);
    %Accumulated distribution
    accHist = DistAcum(clippedhist/(x*y));
    %Equalized histogram
    finalhist = round((L-1)*accHist);
    
    
    % Calculates accumulated distribution
    function [ Hist ] = DistAcum( Hist )
        for i = 2:256
            Hist(i) = Hist(i-1) + Hist(i);
        end
    end

end
