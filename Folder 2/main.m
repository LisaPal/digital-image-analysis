clear all
close all
clc

image = 'hotel.tif'; 
Im= imread(image);  
[height,width] = size(Im);
Rx=1;      % Number of divisions in x (columns)
Ry=1;      % Number of divisions in y (rows)
clip = 0.5; 

 % Check if image is grayscale or color
    [rows , cols , z] = size(Im);
    if z == 1
        fprintf('Grayscale Image\n');
        img = Im;
    elseif z == 3
        fprintf('RGB Image\n');
        HSI = rgbtohsi(Im);
        img = floor(255*HSI(:,:,3));
    end
% 
 [xs,ys]=divide_img(img,Rx,Ry);
 % Uncomment next part if you want to process whole image, without windows
%  img_clahe = clahe(img,clip); 
%  HSI(:,:,3) = img_clahe/255;
%  res = uint8(hsitorgb(HSI));
%  figure;
%  imshow(res,[0 255])
% figure();
%imshow(img_clahe, [0 255])

[newimage]=bilinear_interpol(Im,xs,ys,clip);
imtoshow = newimage; 


if z == 1
    figure;
    res = uint8(imtoshow);
    imshow(res, [0 255])
else
    fprintf('Change back to color image\n');
    HSI(:,:,3) = imtoshow/255;
    res = uint8(hsitorgb(HSI));
    figure;
    imshow(res,[0 255])
end

