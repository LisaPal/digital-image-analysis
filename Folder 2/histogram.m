% returns a normalized histogram's x,y values
function [values, occurrences] = histogram( image )
    values = 0:255; 
    %fprintf('doing histogram');
    for i=0:255
        i;
        occurrences(i+1)= sum(image(:)==i); 
    end 
%     figure();
%     title('Normalized Histogram');
    
    [height,width] = size(image);
    occurrences = occurrences/(height*width); 
    %plot(values,occurrences);
    sumtot = sum(occurrences); 

end

