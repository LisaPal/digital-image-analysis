function [ histoccs ] = clippedHist(img,clip)

% Check if the image is in grayscale or color. If it is a colored image,
% the improvement will be done in the I channel of the HSI color space.If
% its a greyscale image, directly apply the technique on the image. 
L=256;
 
%img= imread(imgfile);  
[height,width] = size(img);

figure(1);
[vals,occs]=histogram(img);
line(get(gca,'Xlim'), [max(occs) max(occs)],'color', 'red')
title('Normalized Histogram');
%legend('Normalized Histogram');
hold on
% Now we need to get the c(k) histogram to find the clipping values 
% rmin and rmax 

for k=1:L
    ck(k) = acc_distr(vals, occs,k); 
end 

%figure(2);
yyaxis right
plot(vals,ck);
title('c(k) accumulated distribution'); 
legend('Normalized Histogram','Histogram Peak','Accumulated Distribution','Location','southeast');

hold off

% From the graph above we can determine the value of clip in range [0,1]
%clip = 0.4; 
% We will clip from the un-normalized histogram
histoccs = occs*(height*width);  % un-normalizing
figure(2)
plot(vals,histoccs);
title('Histogram'); 


threshold = floor(max(histoccs)*clip); 
threshold2 = prctile(histoccs,clip*100);



pixAbove = 0; % count of pixels above threshold
discardbins= zeros(1,256); 
whilecount = 0;
for k=1:L
    whilecount = whilecount +1;
    while (histoccs(k)>=threshold)
        discardbins(1,whilecount)= (k-1);
        %discardbins = [discardbins k-1]; 
        histoccs(k)=histoccs(k)-1; 
        pixAbove = pixAbove+1; 
    end 
        
end

figure(3)
plot(vals,histoccs);
title('Clipped Histogram')

lendisc =length(discardbins(discardbins~=0)) % gives number of bins that we have to discard
pixadd = floor(pixAbove/(256-lendisc)); %number of pixels to add to each bin not including the ones we clipped from
leftover_pix = pixAbove; 
% now we distribute "pixAbove" number of pixels that were above the threshold
% into the 256 possible bins (0-255) 
%newhistoccs = zeros(1,256);
% 
for i=1:L
   % newhistoccs(i) = histoccs(i); 
   % if ~(ismember(vals(i),discardbins))
        if histoccs(i)+ pixadd < threshold
            histoccs(i) = histoccs(i)+ pixadd;
            leftover_pix = leftover_pix - pixadd; 
        end 
    %end
end 



while leftover_pix>0 
    %fprintf('in while\n') 
    randbin = randi([1 256]); 
    if histoccs(randbin)+ 1 <= threshold
        histoccs(randbin) = histoccs(randbin) +1; 
        leftover_pix = leftover_pix-1
    end 
end 


imgnew = zeros(height,width);
for i=1:height
    for j=1:width
        r = img(i,j);
        newval = ceil(map(vals,histoccs/(width*height),r));
        imgnew(i,j)= newval; 
    end    
end 

%  figure(3);
 %[newvals,newoccs] = histogram(imgnew);
% figure();
 warning('off', 'Images:initSize:adjustingMag');
 imshow(imgnew, [0 255]);



% Accumulated distribution function 
% c(k) describes that probability that the kth intensity value has a value
% less than or equal to k. k is a value in vals.
function [ck] = acc_distr(vals, occs,k)
ck = sum(occs(1:floor(k))); 
end

% Mapping function
% The accumulated distribution function scaled times (L-1) 
function [tk] = map(vals,occs,k)
L= 256;
ck=acc_distr(vals, occs,k); 
tk = (L-1)*ck; 
end 




end

