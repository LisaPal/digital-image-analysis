Folder 1: Connected component edge extraction

Folder 2: Adaptive Histogram Equalization for Contrast Enhancement

Folder 3: Frequency domain filtering 

Folder 4: Texture-based segmentation based on Gabor filters

Folder 5: Object classification from shape features 